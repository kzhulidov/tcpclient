#!/usr/bin/env python3

import tkinter as tk
from tkinter import simpledialog
from tkinter.scrolledtext import ScrolledText
import socket
import threading


class Connection:
    def __init__(self, on_data_received):
        self.on_data_received = on_data_received
        self.opened = False

    def open(self, host, port):
        if not self.opened:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((host, port))
            self.listener_thread = threading.Thread(target=self._receive_data)
            self.listener_thread.start()
            self.opened = True

    def close(self):
        if self.opened:
            self.sock.shutdown(socket.SHUT_RDWR)
            self.listener_thread.join()

    def send(self, data):
        if self.opened:
            self.sock.sendall(data)

    def _receive_data(self):
        while True:
            data = self.sock.recv(1024)
            if not data:
                break
            self.on_data_received(data)


class Application(tk.Frame):
    HOST = 'localhost'        # The server's hostname or IP address
    PORT = 5000               # The port used by the server
    FONT = ('consolas', '12')

    def __init__(self):
        root = tk.Tk()
        root.title('TCP Client')
        root.resizable(False, False)
        super().__init__(root)
        self.lock = threading.Lock()
        self.conn = Connection(lambda data: self._append_view_text(data.decode()))
        self.pack()
        self._create_menu(root)
        self._create_widgets()
        self._set_on_close(root)

    def _set_on_close(self, root):

        def close():
            self.conn.close()
            root.destroy()
        root.protocol('WM_DELETE_WINDOW', close)

    def _open_connection(self):
        host, port = None, None
        address = simpledialog.askstring(
            'Open connection',
            'Enter the server address as <host>:<port>',
            parent=self,
            initialvalue=Application.HOST + ':' + str(Application.PORT)
        )
        if address:
            try:
                host, port_str = address.split(':')
                port = int(port_str)
            except ValueError:
                pass
        if host and port:
            self.conn.open(host, port)
            self.conn_menu.entryconfig('Open...', label='Connected to {}:{}'.format(host, port), state='disabled')

    def _create_menu(self, root):
        menubar = tk.Menu(root)
        self.conn_menu = tk.Menu(menubar, tearoff=0)
        self.conn_menu.add_command(label='Open...', command=self._open_connection)
        menubar.add_cascade(label='Connection', menu=self.conn_menu)
        root.config(menu=menubar)

    def _create_widgets(self):
        self.data_view = ScrolledText(self, state=tk.DISABLED, height=40, width=80, font=Application.FONT)
        self.data_view.pack(side='top')

        tk.Label(self, text='Message:', font=Application.FONT).pack(side='left')
        self.client_data = tk.StringVar()
        self.client_data_entry = tk.Entry(self, width=70, textvariable=self.client_data, font=Application.FONT)
        self.client_data_entry.pack(side='left')
        self.client_data_entry.bind('<Key-Return>', lambda evt: self._send_client_data())

    def _send_client_data(self):
        data = self.client_data.get()
        self.client_data.set('')
        self._append_view_text(data)
        self.conn.send(data.encode())

    def _append_view_text(self, text, end='\n'):
        with self.lock:
            self.data_view.config(state=tk.NORMAL)
            self.data_view.insert(tk.END, text + end)
            self.data_view.see(tk.END)
            self.data_view.config(state=tk.DISABLED)


if __name__ == '__main__':
    app = Application()
    app.mainloop()
